import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JColorChooser;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class JavaAssignment extends JFrame {

	JDesktopPane desktop;
	JInternalFrame inFrame;
	JMenuBar menuBar;
	JMenu file, edit;
	JMenuItem new_, open, close, quit, save, saveas, chgColor, pOpen, pClose, pSave;
	JPopupMenu popupMenu;
	JTextPane textPane;
	File currentFile = null;

	public JavaAssignment() {
		desktop = new JDesktopPane();
		inFrame = new JInternalFrame("Editor", true, true, true, true);
		inFrame.setBounds(25, 25, 400, 400);
		menuBar = new JMenuBar();
		popupMenu = new JPopupMenu();
		textPane = new JTextPane();
		file = new JMenu("File");
		edit = new JMenu("Edit");
		new_ = new JMenuItem("New");
		open = new JMenuItem("Open");
		pOpen = new JMenuItem("Open");		
		close = new JMenuItem("Close");
		pClose = new JMenuItem("Close");
		quit = new JMenuItem("Quit");
		save = new JMenuItem("Save");
		pSave = new JMenuItem("Save");
		saveas = new JMenuItem("Save as");
		chgColor = new JMenuItem("Change color");
		start();
	}

	public static void main(String[] args) {
		new JavaAssignment();
	}
	
	private void start() {
		new_.addActionListener(new NewActionListener());
		open.addActionListener(new OpenActionListener());
		save.addActionListener(new SaveActionListener());
		saveas.addActionListener(new SaveAsActionListener());
		close.addActionListener(new CloseActionListener());
		pSave.addActionListener(new SaveActionListener());
		pOpen.addActionListener(new OpenActionListener());
		pClose.addActionListener(new CloseActionListener());
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();

			}
		});
		chgColor.addActionListener(new chgColorActionListener());
		MouseListener showPopup = new MouseAdapter() {

			public void mousePressed(MouseEvent e) {
				if (!e.isPopupTrigger()) {
					return;
				}
				popupMenu.show(e.getComponent(), e.getX(), e.getY());
			}

			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger()) {
					return;
				}
				popupMenu.show(e.getComponent(), e.getX(), e.getY());
			}
		};
		addMouseListener(showPopup);
		textPane.addMouseListener(showPopup);
		file.add(new_);
		file.add(open);
		file.add(save);
		file.add(saveas);
		file.add(close);
		file.add(quit);
		edit.add(chgColor);
		menuBar.add(file);
		menuBar.add(edit);
		popupMenu.add(pOpen);
		popupMenu.add(pSave);
		popupMenu.add(pClose);
		inFrame.add(new JScrollPane(textPane));
		inFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
		desktop.add(inFrame);
		add(desktop, BorderLayout.CENTER);
		setJMenuBar(menuBar);
		setSize(500, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	private class OpenActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			int status = fileChooser.showOpenDialog(desktop);
			if (status != JFileChooser.APPROVE_OPTION) {
				return;
			}
			File file = fileChooser.getSelectedFile();
			String text = null;
			try (FileInputStream stream = new FileInputStream(file)) {
				byte[] buff = new byte[stream.available()];
				stream.read(buff);
				text = new String(buff);
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(desktop, ex.getMessage());
			}
			inFrame.setVisible(true);
			textPane.setText(text);
			currentFile = file;
		}

	}

	private class SaveActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (!inFrame.isVisible()) {
				return;
			}
			if (currentFile == null) {
				saveAs();
				return;
			}
			writeToFile(currentFile);
		}
	}

	private class SaveAsActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (!inFrame.isVisible()) {
				return;
			}
			saveAs();
		}
	}

	private class NewActionListener implements ActionListener {

		public void actionPerformed(ActionEvent ae) {
			inFrame.setVisible(true);
			currentFile = null;
			textPane.setText("");
		}
	}

	private class CloseActionListener implements ActionListener {

		public void actionPerformed(ActionEvent ae) {
			inFrame.setVisible(false);
			currentFile = null;
			textPane.setText("");
		}
	}

	private class chgColorActionListener implements ActionListener {

		public void actionPerformed(ActionEvent ae) {
			JColorChooser colorChooser = new JColorChooser();
			Color color = colorChooser.showDialog(desktop, "Choose background color", Color.WHITE);
			desktop.setBackground(color);
		}

	}

	private void saveAs() {
		JFileChooser fileChooser = new JFileChooser();
		int status = fileChooser.showSaveDialog(desktop);
		if (status != JFileChooser.APPROVE_OPTION) {
			return;
		}
		File file = fileChooser.getSelectedFile();
		writeToFile(file);
		currentFile = file;
	}

	private void writeToFile(File file) {
		try (FileOutputStream stream = new FileOutputStream(file)) {
			byte[] buff = textPane.getText().getBytes();
			stream.write(buff);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(desktop, ex.getMessage());
		}
	}

}
